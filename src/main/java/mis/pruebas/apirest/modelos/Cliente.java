package mis.pruebas.apirest.modelos;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    public String documento;
    public String nombre;
    public String edad;
    public String fechaNacimiento;
    public String telefono;
    public String correo;
    public String direccion;

    // Jackson - Serializacion/Deserializacion JSON
    @JsonIgnore
    public List<Cuenta> cuentas = new ArrayList<Cuenta>();
    public List<String> strCuentas = new ArrayList<>();
}
