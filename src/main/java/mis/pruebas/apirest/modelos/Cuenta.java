package mis.pruebas.apirest.modelos;

import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;

public class Cuenta extends RepresentationModel<Cuenta> {

    public String nroCuenta;
    public BigDecimal importe;
}
