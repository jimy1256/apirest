package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    @Autowired
    ServicioCliente servicioCliente;

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS -> agregarCuentaCliente(documento,cuenta)
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody Cuenta cuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, cuenta);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<List<Cuenta>> obtenerCuentasCliente(@PathVariable String documento) {
        try {
            final List<Cuenta> cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
    }

    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @DeleteMapping
    public ResponseEntity<String> borrarCuentasCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCuentasCliente(documento);
            return ResponseEntity.ok(documento);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{nroCuenta}")
    public ResponseEntity<String> borrarCuentaCliente(@PathVariable String documento, @PathVariable String nroCuenta) {
        try {
            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            if(!cliente.cuentas.contains(nroCuenta)){
                return ResponseEntity.noContent().build();
            }
           // this.servicioCliente.borrarCuentaCliente(documento);
            return ResponseEntity.status(200).build();
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
    }

}
