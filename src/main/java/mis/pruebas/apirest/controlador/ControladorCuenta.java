package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.apache.tomcat.util.http.parser.HttpParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    public ServicioCuenta servicioCuenta;

    @GetMapping
    public List<Cuenta> obtenerCuentas(){
        return servicioCuenta.obtenerCuentas();
    }

    @GetMapping("/{nroCuenta}")
    public Cuenta obtenerCuenta(@PathVariable String nroCuenta){
        try {
            return servicioCuenta.obtenerCuentaPorNroCuenta(nroCuenta);
        }catch(Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Cuenta> insertarCuenta(@RequestBody Cuenta cuenta){
        servicioCuenta.insertarCuenta(cuenta);

        Link link = linkTo(methodOn(this.getClass()).obtenerCuenta(cuenta.nroCuenta)).withSelfRel();
        Link link2 = linkTo(methodOn(this.getClass()).obtenerCuenta(cuenta.nroCuenta)).withSelfRel().withTitle("link2");
        cuenta.add(link, link2);
        return ResponseEntity.status(HttpStatus.CREATED).body(cuenta);
    }

    @PutMapping("/{nroCuenta}")
    public void reemplazarCuenta(@PathVariable String nroCuenta, @RequestBody Cuenta cuenta){
        servicioCuenta.reemplazarCuenta(nroCuenta, cuenta);
    }

    @PatchMapping
    public void actualizarCuenta(@RequestBody Cuenta cuenta){
        this.servicioCuenta.actualizarCuenta(cuenta);
    }

    @DeleteMapping("/{nroCuenta}")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void eliminarCuenta(@PathVariable String nroCuenta){
        try{
            this.servicioCuenta.eliminarCuenta(nroCuenta);
        }catch(Exception e){
        }

    }
}
