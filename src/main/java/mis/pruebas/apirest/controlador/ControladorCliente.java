package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.web.server.ResponseStatusException;

import javax.swing.text.html.parser.Entity;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
//@RequestMapping("${api.rest.url}") --usando application.properties
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping
    public CollectionModel<EntityModel<Cliente>> obtenerClientes(){
        final var clientes =  this.servicioCliente.obtenerClientes();
        final var emClientes = clientes.stream().map(x ->
            EntityModel.of(x).add(linkTo(methodOn(this.getClass()).obtenerClientes()).withSelfRel().withTitle("Cliente: ".concat(x.documento)))
        ).collect(Collectors.toList());

        return CollectionModel.of(emClientes).add(linkTo(methodOn(this.getClass()).obtenerClientes()).withSelfRel().withTitle("Todos los clientes"));
    }

    @PostMapping(produces = { "application/hal+json" })
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente){
        this.servicioCliente.insertarClienteNuevo(cliente);
        //final var uri = linkTo(methodOn(ControladorCliente.class).obtenerCliente(cliente.documento)).toUri();

        final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCliente.class).obtenerCliente(cliente.documento);

        final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();

        return ResponseEntity.ok().location(enlaceEsteDocumento).build();
    }

    //api/v1/clientes/{documento}
    @GetMapping("/{documento}")
    public EntityModel<Cliente> obtenerCliente(@PathVariable String documento){

        try{
            final var cliente = this.servicioCliente.obtenerCliente(documento);
            final var linkCliente = linkTo(methodOn(this.getClass()).obtenerCliente(cliente.documento)).withSelfRel();
            final var linkCuentasCliente = linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(documento)).withSelfRel();
            return EntityModel.of(cliente).add(linkCliente, linkCuentasCliente);
        }catch(Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{documento}")
    public void reemplazarCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente){
        cliente.documento = nroDocumento;
        this.servicioCliente.guardarCliente(cliente);
    }

    @PatchMapping("/{documento}")
    public void emparcharCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente){
        this.servicioCliente.emparcharCliente(cliente);
    }

}
