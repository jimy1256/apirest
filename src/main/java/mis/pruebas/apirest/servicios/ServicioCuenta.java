package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();
    public Cuenta obtenerCuentaPorNroCuenta(String nroCuenta);
    public void insertarCuenta(Cuenta cuenta);
    public void reemplazarCuenta(String nroCuenta, Cuenta cuenta);
    public void actualizarCuenta(Cuenta cuenta);
    public void eliminarCuenta(String nroCuenta);

}
