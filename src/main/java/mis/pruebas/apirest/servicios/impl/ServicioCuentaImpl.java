package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String, Cuenta> cuentas = new ConcurrentHashMap<>();

    @Override
    public List<Cuenta> obtenerCuentas() {
        return this.cuentas.values().stream().collect(toList());
    }

    @Override
    public Cuenta obtenerCuentaPorNroCuenta(String nroCuenta) {

        if(!this.cuentas.containsKey(nroCuenta))
            throw new RuntimeException("No Existe cuenta");

        return this.cuentas.get(nroCuenta);
    }

    @Override
    public void insertarCuenta(Cuenta cuenta) {
        this.cuentas.put(cuenta.nroCuenta, cuenta);
    }

    @Override
    public void reemplazarCuenta(String nroCuenta, Cuenta cuenta) {
        this.cuentas.remove(nroCuenta);
        this.cuentas.put(cuenta.nroCuenta, cuenta);
    }

    @Override
    public void actualizarCuenta(Cuenta cuenta) {
        this.cuentas.replace(cuenta.nroCuenta, cuenta);
    }

    @Override
    public void eliminarCuenta(String nroCuenta) {
        this.cuentas.remove(nroCuenta);
    }
}
