package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import org.springframework.stereotype.Service;
import mis.pruebas.apirest.servicios.ServicioCliente;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    public final Map<String, Cliente> clientes = new ConcurrentHashMap<>();

    @Override
    public List<Cliente> obtenerClientes(){
        return this.clientes.values().stream().collect(toList());
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento, cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        Cliente cliente = this.clientes.get(documento);
        if(cliente == null)
            throw new RuntimeException("Cliente no encontrado");

        return this.clientes.get(documento);
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        this.clientes.replace(cliente.documento, cliente);
    }

    public void emparcharCliente(Cliente parche){
        final Cliente existente = this.clientes.get(parche.documento);

        if(parche.edad != null){
            existente.edad = parche.edad;
        }
    }

    @Override
    public void borrarCliente(String documento) {
        this.clientes.remove(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        cliente.cuentas.add(cuenta);
    }

    @Override
    public List<Cuenta> obtenerCuentasCliente(String documento) {
        final Cliente cliente = this.obtenerCliente(documento);
        return cliente.cuentas;
    }

    @Override
    public void borrarCuentasCliente(String documento) {
        Cliente cliente = this.obtenerCliente(documento);
        cliente.cuentas.clear();
    }

    @Override
    public void borrarCuentaCliente(String documento, String nroCuenta) {
        Cliente cliente = this.obtenerCliente(documento);
        for(Cuenta cuenta : cliente.cuentas){
            if(nroCuenta.equals(cuenta.nroCuenta)){
                //cliente.cuentas.
            }
        }
    }
}
