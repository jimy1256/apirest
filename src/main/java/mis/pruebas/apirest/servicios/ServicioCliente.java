package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCliente {
    //CRUD
    public List<Cliente> obtenerClientes();

    //CREATE
    public void insertarClienteNuevo(Cliente cliente);

    //READ
    public Cliente obtenerCliente(String documento);

    public void guardarCliente(Cliente cliente);

    public void borrarCliente(String documento);

    public void emparcharCliente(Cliente parche);

    public void agregarCuentaCliente(String documento, Cuenta cuenta);

    public List<Cuenta> obtenerCuentasCliente(String documento);

    public void borrarCuentasCliente(String documento);

    public void borrarCuentaCliente(String documento, String nroCuenta);
}
